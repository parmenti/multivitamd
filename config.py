import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'dup9;R%.51s9 2e]RdI@k|\\5'
    #DATABASE_URL = 'postgresql://postgresql-piaf.alwaysdata.net/piaf_editor?user=piaf&password=ue908.ue908'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    LANGUAGES = ['fr', 'en']
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    REMEMBER_COOKIE_SECURE = True
