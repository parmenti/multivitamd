/**
 * JS file used to handle MD preview in editor.html and example.html
 * Used to display preview
**/
autoPreview()

function autoPreview() {
    mdPreview();
    setTimeout(autoPreview, 1000);
}

/**
 * Converts edited text in md to HTML and displays it
 */
function mdPreview() {
    let refLang = document.getElementById("lang0").value;
    if (document.getElementById("content") === null)
        toasts("Une erreur est survenue lors du traitement de votre document.\nPour conserver vos données, merci d'effectuer un copié-collé. ");
    else {
        let content = document.getElementById("content").value;
        let pre = document.getElementById("currentText");
        pre.innerHTML = (converter.makeHtml(content));
    }
}
