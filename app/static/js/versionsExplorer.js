// This JS is used on the /files/documents/explore page
// It is used to display files and to restore a selected version


// fileDirectory is used to identify current file
let parts = window.location.href.split('/');
let fileDirectory = parts.pop() || parts.pop();
fileDirectory = fileDirectory.replace("#", "");


// When loading the page, we select first version
if (document.getElementById("select-1") !== null) {
    selectVersion(document.getElementById("select-1"))
}

/*
 This function is called to change the version displayed
 It asks the file content to the server
  It also indicate the selection
 */
function selectVersion(elem) {
    document.getElementById("body").style.cursor = "progress";
    document.getElementById("statut").innerHTML = "<div class='spinner-border spinner-border-sm text-light' role='status'></div> <span> Chargement en cours...</span>";

    // unselects all previously selected versions
    let displayed = document.getElementsByClassName("versionSelected");
    for (let i = 0; i < displayed.length; i++) {
        displayed[i].classList.remove("versionSelected");
    }

    // selects current version
    elem.classList.add("versionSelected");

    let version = elem.getAttribute("value");

    // set value to restore button
    document.getElementById("restore-btn").value = version;

    makeRequest("/files/getDocument/", "directory=" + fileDirectory + "&versionId=" + version, printVersion)

}

function printVersion() {
    if (this.readyState === XMLHttpRequest.DONE) {
        if (this.status === 500) {
            toasts("Une erreur est apparue lors de l'accès à cette version. Réessayez plus tard.");
        } else {
            let resp = this.response;
            if (resp === "err")
                toasts("Une erreur est apparue lors de l'accès à cette version. Réessayez plus tard.");
            else {
                // convert response to JSON object
                f = JSON.parse(resp);
                let file = f.file;
                let infos = f.infos
                let langs = infos.languages

                // Displays file name
                document.getElementById("fileName").innerText = infos.title

                // Displays languages
                let l0 = document.getElementsByClassName("lang0")
                for (let i = 0; i < l0.length; i++) {
                    l0[i].innerText = langs[0]
                }
                let l1 = document.getElementsByClassName("lang1")
                for (let i = 0; i < l1.length; i++) {
                    l1[i].innerText = langs[1]
                }
                let l2 = document.getElementsByClassName("lang2")
                for (let i = 0; i < l2.length; i++) {
                    l2[i].innerText = langs[2]
                }

                // Preparing texts
                let text0 = ""
                let text1 = ""
                let text2 = ""
                let down = file
                while (down["down"] !== undefined) {
                    down = down["down"]
                    text0 += down["content"][langs[0]]
                    text1 += down["content"][langs[1]]
                    text2 += down["content"][langs[2]]
                }

                if (text0 === "")
                    text0 = "<span class='text-warning font-italic'>Aucun texte à afficher</span>"
                if (text1 === "")
                    text1 = "<span class='text-warning font-italic'>Aucun texte à afficher</span>"
                if (text2 === "")
                    text2 = "<span class='text-warning font-italic'>Aucun texte à afficher</span>"

                // Displays texts
                document.getElementById("text0").innerHTML = converter.makeHtml(text0)
                document.getElementById("text1").innerHTML = converter.makeHtml(text1)
                document.getElementById("text2").innerHTML = converter.makeHtml(text2)
            }
        }
        document.getElementById("body").style.cursor = "auto";
        document.getElementById("statut").innerHTML = "<span> Chargement réussi</span>"
        setTimeout(eraseStatut, 20000)
    }
}

/**
 * Function called to restore an identified version
 */
function restoreVersion(elem) {
    makeRequest('/restore/', 'directory=' + fileDirectory + "&idV=" + elem.value, hideVersions)
}


function hideVersions() {
    $('#restoreModal').modal('toggle');
    if (this.readyState === XMLHttpRequest.DONE) {
        if (this.status === 500) {
            toasts("Erreur serveur inconnue.");
        } else {
            let resp = this.response;
            if (resp === "err")
                toasts("Impossible de restaurer votre versions.");
            else {
                let tab = resp.split("-")
                for (let i = 0; i < tab.length; i++) {
                    document.getElementById("select-" + tab[i]).remove()
                }
            }
        }

    }
}