let parts = window.location.href.split('/');
// fileDirectory is used to identify current file
let fileDirectory = parts.pop() || parts.pop();
fileDirectory = fileDirectory.replace("#", "");

let newlyOpenend = true;


if (document.getElementById("save") !== null) {
    //On s'assure que l'utilisateur est sûr de vouloir quitter/actualiser la page
    window.onbeforeunload = function (event) {
        event.returnValue = "Voulez-vous quitter?";
    };

    //On activera la sauvegarde auto dans 1 min
    if (document.getElementById("autoSave").checked)
        setTimeout(autoSave, 60000);
    document.getElementById("save").onclick = function () {
        saveContent();
    };

    //On active la prévisualisation auto
    autoPreview();

    makeRequest('/files/getDocument/', "directory=" + fileDirectory, updateFile);

    //On regarde la langue sélectionnée et on applique
    document.getElementById("selectLang").onchange = function () {
        if (document.getElementById("selectLang").value !== "-1")
            makeRequest('/files/getDocument/', "directory=" + fileDirectory, updateFile)
    }
}

// Update file infos
if (document.getElementById("submitFileChanges") !== null) {
    document.getElementById("submitFileChanges").onclick = function () {
        let fileName = document.getElementById("fileName").value;
        if (fileName === "")
            fileName = "file";

        let langs = document.getElementsByName("newLang");
        for (let i = 0; i < langs.length; i++) {
            if (langs[i].value === "")
                langs[i].value = Math.random().toString(36);
        }
        let lang0Name = langs[0].value;
        let lang1Name = langs[1].value;
        let lang2Name = langs[2].value;

        if (lang0Name !== lang1Name && lang0Name !== lang2Name && lang1Name !== lang2Name) {
            let txt = "<div class='alert alert-info' role='alert'><div class='spinner-grow spinner-grow-sm' role='status'></div> " +
                "Mise à jour en cours..." +
                "</div>"
            document.getElementById("updateAlert").innerHTML = txt
            let data = "directory=" + fileDirectory + "&fileName=" + fileName + "&lang0=" + lang0Name + "&lang1=" + lang1Name + "&lang2=" + lang2Name;
            makeRequest('/updateFile/', data, updateFile)
        } else {
            let txt = "<div class='alert alert-danger' role='alert'>\n<i class='fas fa-exclamation-circle'></i>" +
                "  Les langues du document doivent être différentes les unes des autres." +
                "\n</div>"
            document.getElementById("updateAlert").innerHTML = txt
        }
    };
}

function saveContent() {
    document.getElementById("content").disabled = true;
    document.getElementById("content").style.cursor = "progress";
    document.getElementById("selectLang").disabled = true;
    document.getElementById("selectLang").style.cursor = "not-allowed";

    document.getElementById("statut").innerHTML = "<div class='spinner-border spinner-border-sm text-light' role='status'></div> Sauvegarde en cours...</span>";
    if (document.getElementById("content") === null || document.getElementById("selectLang") === null)
        toasts("Une erreur est survenue lors de l'enregistrement de votre document.\nPour conserver vos données, merci d'effectuer un copié-collé. ");
    else {
        let lang = document.getElementById("selectLang").value;
        if (lang !== "-1") {
            let sameLevelTitle = document.getElementById("sameLevelTitle").checked
            let content = document.getElementById("content").value;

            let file = document.getElementById("file").value
            makeRequest('/save/', "file=" + file + "&directory=" + fileDirectory + "&content=" + content + "&currentLang=" + lang + "&sameLevelTitle=" + sameLevelTitle, printSaved);

        } else
            document.getElementById("statut").innerText = "Échec de la sauvegarde: sélectionnez une langue";
    }
}


/**
 * Update displayed file
 */
function updateFile() {
    if (this.readyState === XMLHttpRequest.DONE) {
        if (this.status === 500)
            toasts("Une erreur est survenue lors de l'ouverture de votre document.\nLes données de votre document sont peut-être corrompues.");
        else {
            let txt = this.response;
            if (txt === "err")
                toasts("Une erreur est survenue lors de l'ouverture de votre document.");
            else {
                $('#fileModal').modal('hide');
                document.getElementById("updateAlert").innerHTML = ""
                printText(txt);
            }
        }
    }
}


function autoSave() {
    if (document.getElementById("autoSave").checked === true) {
        saveContent();
    }
    saveContent();
    setTimeout(autoSave, 300000);
}


/**
 * Diplays save infos
 */
function printSaved() {
    if (this.readyState === XMLHttpRequest.DONE) {
        document.getElementById("content").disabled = false;
        document.getElementById("content").style.cursor = "auto";
        document.getElementById("selectLang").disabled = false;
        document.getElementById("selectLang").style.cursor = "auto";

        let statut = document.getElementById("statut");
        if (this.status === 500) {
            toasts("Une erreur est survenue lors de l'enregistrement de votre document.\nLes données de votre document sont peut-être corrompues.\nPour conserver vos données, merci d'effectuer un copié-collé.");
            statut.innerHTML = "<span><i class='fas fa-exclamation-circle'></i> Échec de la sauvegarde</span>"
        } else {
            let file = this.response;

            if (file === "err")
                toasts("Une erreur est survenue lors de l'enregistrement de votre document.\nPour conserver vos données, merci d'effectuer un copié-collé. ");
            else {
                // Mise à jour du statut du document
                let now = new Date();
                let heure = now.getHours();
                let minute = now.getMinutes();
                statut.innerHTML = "<span>Dernière sauvegarde : " + heure + ":" + minute + "</span>";

                printText(file);

            }
        }
    }
}

/**
 * Displays file received
 * Focusing on file infos
 * File "content" is assigned as it is and is not shown
 * @param fileText
 */
function printText(fileText) {
    if (newlyOpenend) {
        document.getElementById("statut").innerHTML = "<span>Chargement réussi</span>";
        setTimeout(eraseStatut, 20000);
        newlyOpenend = false;
    }
    f = JSON.parse(fileText);
    let file = f.file;
    let infos = f.infos
    let tmpLangs = infos.languages
    let langs = []
    langs[0] = tmpLangs[0]
    langs[1] = tmpLangs[1]
    langs[2] = tmpLangs[2]


    let l0 = document.getElementsByClassName("lang0")
    for (let i = 0; i < l0.length; i++) {
        l0[i].value = langs[0]
        l0[i].innerText = langs[0]
    }

    let l1 = document.getElementsByClassName("lang1")
    for (let i = 0; i < l1.length; i++) {
        l1[i].value = langs[1]
        l1[i].innerText = langs[1]
    }

    let l2 = document.getElementsByClassName("lang2")
    for (let i = 0; i < l2.length; i++) {
        l2[i].value = langs[2]
        l2[i].innerText = langs[2]
    }

    let fn = document.getElementById("fileName")
    fn.value = infos["title"]

    document.title = infos["title"]

    let lang0 = document.getElementById("selectLang").value;
    langs.splice(langs.indexOf(lang0), 1);
    let lang1 = langs[0];
    let lang2 = langs[1];

    document.getElementById("file").innerText = fileText;
    document.getElementById("currentLang").innerText = lang0;
    document.getElementById("lang1Name").innerText = lang1;
    document.getElementById("lang2Name").innerText = lang2;

    updateContent();
    referenceDisplay(lang0, lang1, lang2);
}

/**
 * content update for current lang (edited at this time)
 */
function updateContent() {
    let f = document.getElementById("file").value;
    f = JSON.parse(f);
    let file = f.file
    let lang0 = document.getElementById("selectLang").value;

    let txt = "";
    while (file.down != null) {
        file = file.down;
        let cont = file.content;
        txt += cont[lang0];
    }
    document.getElementById("content").value = txt;
}

/**
 * Displays reference texts
 */
function referenceDisplay(lang0, lang1, lang2) {
    let file = JSON.parse(document.getElementById("file").value);
    file = file.file;

    let txt1 = "";
    let txt2 = "";
    while (file.down != null) {
        file = file.down;
        let upd0 = file.lastUpdate[lang0];

        let upd1 = file.lastUpdate[lang1];
        let line1 = file.content[lang1];
        txt1 += prepareLine(line1, upd0, upd1);

        let upd2 = file.lastUpdate[lang2];
        let line2 = file.content[lang2];
        txt2 += prepareLine(line2, upd0, upd2);
    }

    document.getElementById("langP1").innerHTML = txt1;
    document.getElementById("langP2").innerHTML = txt2;
}

/**
 * Used to
 * @param text represents the line
 * @param upd0 updates of currently edited language
 * @param updc updates of currently prepared line
 * @returns {string}
 */
function prepareLine(text, upd0, updc) {
    let res = "";
    if (text === "" || text === " " || text === "\n")
        text = "¶\n"
    if (Date.parse(upd0[1]) < Date.parse(updc[1])) {
        switch (updc[0]) {
            case "add":
                res += "<div class='text-success'>" + converter.makeHtml(text) + "</div>";
                break;
            case "del":
                res += "<div class='text-danger'>" + converter.makeHtml(text) + "</div>";
                break;
            case "edit":
                res += "<div class='text-warning'>" + converter.makeHtml(text) + "</div>";
                break;
            default:
                res += "<div class='text-dark'>" + converter.makeHtml(text) + "</div>";
                break;
        }
    } else {
        res += "<div class='text-dark'>" + converter.makeHtml(text) + "</div>";
    }
    return res;
}

