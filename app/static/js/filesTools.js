localStorage[0] = 0;
localStorage['directoryFile'] = ""

function deleteFile(elem) {
    $('#delete-modal-' + elem.value).modal('toggle');
    makeRequest('/deletefile/', 'path=' + elem.value, postDelete)
}

function postDelete() {
    if (this.readyState === XMLHttpRequest.DONE) {
        if (this.status === 500)
            toasts("Ce document ne peut être supprimé.");
        else {
            reload();
        }
    }
}

function shareFile(btn) {
    let directoryFile = btn.value
    localStorage['directoryFile'] = directoryFile
    let idF = btn.id.split('-')[1];
    localStorage[0] = idF;
    let username = document.getElementById("shareInput-" + idF).value;
    if (username === "") {
        let print = "<div class='alert alert-warning' role='alert'><i class='far fa-times-circle'></i> Vous n'avez pas indiqué.e d'identifiant.</div>";
        document.getElementById("shared-result-" + localStorage[0]).innerHTML = print;
    } else {
        let print = "<div class='alert alert-secondary' role='alert'><div class='spinner-border spinner-border-sm' role='status'></div> Chargement en cours.</div>";
        document.getElementById("shared-result-" + localStorage[0]).innerHTML = print;
        makeRequest('/sharefile/', 'directory=' + directoryFile + "&idF=" + idF + "&username=" + username, printSharedUnshared);
    }
}

function printSharedUnshared() {
    if (localStorage[0] !== 0) {
        let print = "";
        if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status === 404)
                print = "<div class='alert alert-danger' role='alert'><i class='far fa-times-circle'></i> Une erreur est survenue lors du traitement de votre demande. Réessayez plus tard.</div>";
            else
                print = this.response
            document.getElementById("shared-result-" + localStorage[0]).innerHTML = print;
        }
    }
}

function unshareFile(btn) {
    let idF = btn.id.split('-')[1];
    localStorage[0] = idF;
    let print = "<div class='alert alert-secondary' role='alert'><div class='spinner-border spinner-border-sm' role='status'></div> Chargement en cours.</div>";
    document.getElementById("shared-result-" + localStorage[0]).innerHTML = print;
    makeRequest('/unsharefile/', "idF=" + idF, printSharedUnshared)
}

function unshareFileWith(e) {
    let idF = e.id.split('-')[2];
    localStorage[0] = idF;
    let idU = e.id.split('-')[1];
    let print = "<div class='alert alert-secondary' role='alert'><div class='spinner-border spinner-border-sm' role='status'></div> Chargement en cours.</div>";
    document.getElementById("shared-result-" + localStorage[0]).innerHTML = print;
    makeRequest('/unsharefile/', "idF=" + idF + "&idU=" + idU, printSharedUnshared)
}

function hideFile(e) {
    makeRequest('/unsharefile/', "idF=" + e.value + "&idU=self", window.reload)
}

function orderFiles(elem) {
    document.cookie = "orderFiles=" + elem.value;
    reload()
}