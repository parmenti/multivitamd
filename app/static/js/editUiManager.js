/**
 * Used to switch from a language to another
 * @param e
 */
function selectLang(e) {
    document.getElementById("lang1Name").className = "font-weight-normal"
    document.getElementById("lang2Name").className = "font-weight-normal"
    e.className = "font-weight-bold"
    if (e.id === "lang1Name"){
        document.getElementById("langP1").style.display = "block"
        document.getElementById("langP2").style.display = "none"
    }
    if (e.id === "lang2Name"){
        document.getElementById("langP2").style.display = "block"
        document.getElementById("langP1").style.display = "none"
    }
}