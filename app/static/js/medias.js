/**
 * JS file used by /medias/<type>
 * Mostly used because of the add new media form
 */

/**
 * Checks if a link or a file is provided
 * If it is the case: disable the other entry
 */
function checkSource(elem) {
    let link = document.getElementById("Link")
    let file = document.getElementById("File")
    if (elem === link) {
        file.toggleAttribute("disabled")
    }
    if (elem === file) {
        link.toggleAttribute("disabled")
    }
}

/**
 * Prepare request and upload image
 */
function addImage() {
    let link = document.getElementById("Link")
    let file = document.getElementById("File")
    let name = document.getElementById("Name")
    let pub = document.getElementById("Public")
    let check = formComplete(link, file, name)
    if (check) {
        $('#addModal').modal('toggle');
        // if there is no file (meaning there is a link)
        if (file.files.length === 0) {
            makeRequest('/addImage/', "name=" + name.value + "&link=" + encodeURI(link.value) + "&public=" + pub.checked, reload())
        } else {
            let formData = new FormData();
            let file = document.getElementById('File').files[0];
            let xhr = new XMLHttpRequest();
            xhr.onreadystatechange = reload;
            formData.append('file', file);
            formData.append('name', name.value)
            formData.append('public', pub.checked)
            xhr.open('POST', '/addImage/');
            xhr.send(formData);
        }

    }
}

/**
 * Checks if form is fullfield
 * @param link
 * @param file
 * @param name
 */
function formComplete(link, file, name) {
    let res = true
    if (link.value === "" && file.files.length === 0) {
        res = false
        link.classList.remove("is-valid")
        file.classList.remove("is-valid")
        link.classList.add("is-invalid")
        file.classList.add("is-invalid")
    } else {
        link.classList.remove("is-invalid")
        file.classList.remove("is-invalid")
        link.classList.add("is-valid")
        file.classList.add("is-valid")
    }
    if (name.value === "") {
        res = false
        name.classList.remove("is-valid")
        name.classList.add("is-invalid")
    } else {
        name.classList.remove("is-invalid")
        name.classList.add("is-valid")
    }
    if (!link.hasAttribute("disabled") && !isUrl(link.value)) {
        res = false
        link.classList.add("is-invalid")
    }
    return res
}

/**
 * Copy Media to clipboard
 * Media => MD tag
 * @param elem
 */
function copyToClipboard(elem) {
    let copyText = document.getElementById("copy-" + elem.id);

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
    toasts("Média copié. Collez le dans votre document.")
}

/**
 * Function called to delete a media
 */
function deleteMedia(elem) {
    $('#addModal').modal('toggle');
    makeRequest('/deleteImage/', "id=" + elem.value, reload())
}

/**
 * Change public statut of a media
 * @param elem
 */
function public(elem) {
    makeRequest('/publicStatutMedia/', "id=" + elem.value + "&public=" + elem.checked, null)
}