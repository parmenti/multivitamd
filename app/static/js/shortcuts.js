

// save
Mousetrap.bindGlobal(['shift+command+s', 'shift+ctrl+s'], saveContent);

// export
Mousetrap.bindGlobal(['shift+command+e', 'shift+ctrl+e'],);

// file options
Mousetrap.bindGlobal(['shift+command+f', 'shift+ctrl+f'], function (e) {
    $('#fileModal').modal('show');
});

// help
Mousetrap.bindGlobal(['shift+command+h', 'shift+ctrl+h', 'f1'], function (e) {
    $('#helpModal').modal('show');
});

// open
Mousetrap.bindGlobal(['shift+command+o', 'shift+ctrl+o'], function (e) {
    window.location = "/files/"
});

// create file
Mousetrap.bindGlobal(['shift+command+c', 'shift+ctrl+c'], function (e) {
    window.location = "/newFile/"
});

