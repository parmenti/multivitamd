/**
 * JS related to export function
 **/


/**
 * updates button
 **/
function updateButton(elem) {
    document.getElementById("outputSelected").innerText = elem.value
}

/**
 * function called to export file
 * Makes a request to documents/export/<directory>
 */
function exportDocument() {
    document.getElementById("exportNormal").classList.add("d-none")
    document.getElementById("exportLoading").classList.remove("d-none")

    let directory = fileDirectory
    let output = document.getElementById("selectOutput").value
    makeRequest('/files/documents/export/', "directory=" + directory + "&output=" + output, postExport)
}

/**
 * function called to download an archive
 */
function archiveDocument() {
    document.getElementById("archiveNormal").classList.add("d-none")
    document.getElementById("archiveLoading").classList.remove("d-none")
    let directory = fileDirectory
    makeRequest('/files/documents/archive/', "directory=" + directory, postExport)

}

function postExport() {
    if (this.readyState && this.response !== "") {
        document.getElementById("exportNormal").classList.remove("d-none")
        document.getElementById("exportLoading").classList.add("d-none")
        document.getElementById("archiveNormal").classList.remove("d-none")
        document.getElementById("archiveLoading").classList.add("d-none")

        // response is the new fileName
        window.location = "/files/documents/export/download/" + this.response
    }
}