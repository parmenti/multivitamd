/**
 * This file is used by /admin to controls users
 **/

/**
 * function reseting a user's password
 * @param elem
 */
function resetPassword(elem) {
    let pass = document.getElementById("newPassword-" + elem.value).value
    pass = encodeURI(pass)
    // close modal
    $('.modal').modal('hide');
    document.getElementById("statut").innerHTML = "<div class='spinner-border spinner-border-sm text-light' role='status'></div> <span>Reseting paword...</span>"
    makeRequest('/admin/resetPassword/', "id=" + elem.value + "&password=" + pass, statutAdmin)
}

/**
 * Asks server to delete an account
 * @param elem
 */
function deleteAccount(elem) {
    $('.modal').modal('hide');
    document.getElementById("statut").innerHTML = "<div class='spinner-border spinner-border-sm text-light' role='status'></div> <span>Deleting account...</span>"
    makeRequest('/admin/deleteAccount/', "id=" + elem.value, statutAdmin)
}

/**
 * Asks server to make a user admin
 * @param elem
 */
function removeAdmin(elem) {
    $('.modal').modal('hide');
    document.getElementById("statut").innerHTML = "<div class='spinner-border spinner-border-sm text-light' role='status'></div> <span>Making admin...</span>"
    makeRequest('/admin/role/make/user', "id=" + elem.value, statutAdmin)
}

/**
 * Asks server to make a user simple user
 * @param elem
 */
function makeAdmin(elem) {
    $('.modal').modal('hide');
    document.getElementById("statut").innerHTML = "<div class='spinner-border spinner-border-sm text-light' role='status'></div> <span>Removing admin...</span>"
    makeRequest('/admin/role/make/admin', "id=" + elem.value, statutAdmin)
}

/**
 * Displays update status
 */
function statutAdmin() {
    if (this.readyState) {
        if (this.status === 200) {
            flash(this.response)
        }
        if (this.status === 500){
            flash("Error while updating database.")
        }
        reload()
    }
}