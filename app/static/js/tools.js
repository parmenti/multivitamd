var converter = new showdown.Converter({
    "requireSpaceBeforeHeadingText": true,
    "noHeaderId": true,
    "simplifiedAutoLink": false,
    "tables": false,
    "tasklists": true,
    "simpleLineBreaks": true
});

// used to prepare toasts
$('.toast').toast('show');


function flash(string) {
    makeRequest('/flash/', 'flash=' + string, null);
}

function toasts(string) {
    let div = document.getElementById("toastHere");
    let id = "toast" + Math.round(Math.random() * 1000);
    div.innerHTML = "<div id = '" + id + "' class='toast' data-delay='30000' data-animation='true' role='alert' aria-live='assertive'\n" +
        "                         aria-atomic='true'>\n" +
        "                        <div class='toast-header'>\n" +
        "                            <strong class='mr-auto'><img src='/static/img/icon.png' alt='MD'\n" +
        "                                                         width='20' height='20'>\n" +
        "                                MultivitaMD</strong>\n" +
        "                            <button type='button' class='ml-2 mb-1 close' data-dismiss='toast'>\n" +
        "                                <span aria-hidden='true'>&times;</span>\n" +
        "                            </button>\n" +
        "                        </div>\n" +
        "                        <div class='toast-body'>\n" +
        "                            " + string +
        "                        </div>\n" +
        "                    </div>"
    ;
    $("#" + id).toast('show');
}

function makeRequest(url, data, then) {
    let req = new XMLHttpRequest();
    req.onreadystatechange = then;
    req.open("POST", url);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(data);
}

function getUrl(to) {
    makeRequest('/urlfor/', 'to=' + to, function (e) {
        if (this.readyState === XMLHttpRequest.DONE) {
            return this.response;
        }
    });
}

function reload() {
    location.reload()
}

function eraseStatut() {
    document.getElementById("statut").innerHTML = "<span></span>"
}

// Converts MD to HTML
md = document.getElementsByClassName("markdown")
for (let i = 0; i < md.length; i++) {
    md[i].innerHTML = converter.makeHtml(md[i].innerText)
}

// Select language displayed:
function setLang(elem) {
    let lang = elem.value
    document.cookie = "lang=" + lang + "; path=/";
    reload()
}

// Checks if a string is a URL inspired by w3resource
function isUrl(str) {
    let regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(str)) {
        return true;
    } else {
        return false;
    }
}