from flask_babel import _

errorTab = {"MvMd:err-missing-directory": _("Document indisponible.")
    , "MvMd:err-missing-versions": _("Aucunne version.")}
