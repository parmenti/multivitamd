import os, sys

from flask import Flask, request, session
from flask_babel import Babel
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
#from pypandoc.pandoc_download import download_pandoc
from init_db import upgrade

from config import Config

app = Flask(__name__)
app.config.from_object(Config)

# DB manager
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Login manager
login = LoginManager(app)
login.login_view = 'login'

# directories
files_dir = os.path.join(app.instance_path, 'files')
os.makedirs(files_dir, exist_ok=True)
medias_dir = os.path.join(app.instance_path, 'medias')
os.makedirs(medias_dir, exist_ok=True)
temp_dir = os.path.join(app.instance_path, 'temporary')
os.makedirs(temp_dir, exist_ok=True)

@app.cli.command("check")
def check():
    print('**',app.instance_path)

# Localisation manager
babel = Babel(app)

# Return display lang
@babel.localeselector
def get_locale():
    if request.cookies.get('lang') is not None:
        session['lang'] = request.cookies.get('lang')
        return session['lang']
    elif 'lang' in session:
        return session['lang']
    else:
        session['lang'] = request.accept_languages.best_match(app.config['LANGUAGES'])
        return session['lang']


# Init db
@app.cli.command("init_db")
def init_db():
    db.init_app(app)
    db.create_all()#upgrade(db)
    #with app.app_context():
        #db.drop_all()
        #db.create_all()
        #db.session.add(User(1,'admin', 'yannick.parmentier@univ-lorraine.fr', '', 'français', 'english', 'deutsch', "admin"))
        #db.session.commit()
    print("db init done")

# Export manager
#download_pandoc()

# used to check file compatibility
version = "0.0.5"

# os.system('python ' + app.instance_path + '/../cleaner.py ' + app.instance_path + " &")

from app import routes
from app.modelControler import models
