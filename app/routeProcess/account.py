from flask import flash, redirect, url_for, render_template, request
from flask_login import current_user, logout_user, login_user
from flask_babel import _
from werkzeug.urls import url_parse

from app import db, files_dir
from app.form import RegistrationForm, LoginForm, EditAccountForm
from app.modelControler.controlers import deleteAccount
from app.modelControler.models import User


def accLogout():
    usr = current_user.username
    logout_user()
    flash(_('Vous avez été déconnecté.e. À bientôt %(usr)s !', usr=usr))
    del usr
    return redirect(url_for('index'))


def accRegister():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        err = False
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None:
            err = True
            flash(_('Identifiant déjà utilisé. Utilisez-en un autre.'))
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None:
            err = True
            flash(_('Courriel déjà utilisé. Utilisez-en un autre.'))
        if err:
            return redirect(url_for('register'))
        else:
            user = User(username=form.username.data, email=form.email.data)
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()
            flash(_('Votre compte a bien été créé!'))
            return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


def accLogin():
    if current_user.is_authenticated:
        flash(_('Vous avez été redirigé.e.'))
        return redirect('/')
    form = LoginForm()
    if form.validate_on_submit():
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')

        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash(_('Identifiant ou mot de passe incorrect.'))
            return redirect(url_for('login', next=next_page))
        login_user(user, remember=form.remember_me.data)
        flash(_('Bienvenue %(usr)s. Vous êtes bien connecté.e.', usr=user.username))

        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


def accAccount():
    form = EditAccountForm()
    us = current_user
    if form.delete.data:
        if current_user.check_password(form.oldPassword.data):
            return deleteAccount(files_dir)
        else:
            flash(_("Mot de passe incorrect."))

    if form.submit.data:
        err = False
        if not current_user.check_password(form.oldPassword.data):
            err = True
            flash(_("Mot de passe incorrect."))
        if form.lang0.data == form.lang1.data and form.lang0.data is not "" and form.lang1.data is not "":
            err = True
            flash(_("La première et la deuxième langue doivent être différentes."))
        if form.lang0.data == form.lang2.data and form.lang0.data is not "" and form.lang2.data is not "":
            err = True
            flash(_("La première et la troisième langue doivent être différentes."))
        if form.lang2.data == form.lang1.data and form.lang2.data is not "" and form.lang1.data is not "":
            err = True
            flash(_("La deuxième et la troisième langue doivent être différentes."))
        if form.password.data != form.password2.data:
            err = True
            flash(_("Impossible de changer le mot de passe. Les deux champs sont différents."))
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.id is not current_user.id:
            err = True
            flash(_('Identifiant déjà utilisé. Choisissez-en un autre.'))
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.id is not current_user.id:
            err = True
            flash(_('Courriel déjà utilisé. Choisissez-en un autre.'))

        if not err:
            user = User.query.filter_by(username=current_user.username).first()
            user.username = form.username.data
            user.email = form.email.data
            user.lang0 = form.lang0.data
            user.lang1 = form.lang1.data
            user.lang2 = form.lang2.data
            if form.password.data is not "":
                user.set_password(form.password.data)
            db.session.commit()
            login_user(user)
            return redirect(url_for("account"))

    return render_template('account.html', form=form, user=us)
