import os

from flask import url_for, request, render_template, flash, abort, send_from_directory
from flask_login import current_user
from werkzeug.utils import redirect, secure_filename

from app import medias_dir, db
from app.modelControler.controlers import checkImageAccess
from app.modelControler.models import Media
from app.process.tools import rand

from flask_babel import _


# displays medias
def medMedias(type):
    if type == "private":
        m = Media.query.filter_by(idU=int(current_user.id))

    elif type == "public":
        m = Media.query.filter_by(public=True)

    else:
        return redirect(url_for('medias', type="private"))

    # Order by
    order = "recent"
    if "orderFiles" in request.cookies:
        order = request.cookies["orderFiles"]
    if order == "recent-1":
        m = m.order_by(Media.create.asc()).all()
    elif order == "alpha":
        m = m.order_by(Media.name.asc()).all()
    elif order == "alpha-1":
        m = m.order_by(Media.name.desc()).all()
    else:
        m = m.order_by(Media.create.desc()).all()
    return render_template('medias.html', type=type, order=order, medias=m)


# adds an image
def medAddImage():
    if 'name' in request.form and 'public' in request.form:
        if request.form['public'] == "true":
            pub = True
        else:
            pub = False

        if 'link' in request.form:
            media = Media(idU=current_user.id, name=request.form['name'], path=request.form['link'], public=pub)
        elif 'file' in request.files:
            f = request.files['file']
            path = str(rand()) + "-" + secure_filename(f.filename)
            full_path = os.path.join(medias_dir, path)
            #media = Media(idU=current_user.id, name=request.form['name'], path=url_for("accessImage", path=path), public=pub)
            media = Media(idU=current_user.id, name=request.form['name'], path=os.path.join(medias_dir, path), public=pub)
            f.save(medias_dir + "/" + path)
        else:
            flash(_("Erreur lors de l'ajout de l'image, réessayez plus tard."))
            return "err: missing argument.s"
        flash(_("Image ajoutée."))
        db.session.add(media)
        db.session.commit()
        return "Success"
    else:
        flash(_("Erreur lors de l'ajout de l'image, réessayez plus tard."))
        return "err: missing argument.s", 500


def medDelete():
    media = Media.query.filter_by(id=request.form["id"]).first()
    if media is None:
        flash(_("Ce média n'existe pas'"))
        return "err: not your media"
    if media.idU is not current_user.id:
        flash(_("Ce média ne vous appartient pas. Vous ne pouvez pas l'effacer."))
        return "err: not your media"
    tmp = media.path.split("/")
    path = tmp[len(tmp)-1]
    print(medias_dir + "/" + path)
    if os.path.isfile(medias_dir + "/" + path):
        os.remove(medias_dir + "/" + path)
    db.session.delete(media)
    db.session.commit()
    flash(_("Votre média a été effacé."))
    return "Success"


def medAccess(path):
    if not os.path.isfile(medias_dir + "/" + path):
        abort(404)
    if checkImageAccess(os.path.join(medias_dir, path)):#checkImageAccess(url_for("accessImage", path=path)):
        return send_from_directory(medias_dir, path, as_attachment=False)
    else:
        return "ACCESS FORBIDEN: not your image", 403


def medPublicStatut():
    if 'id' in request.form and 'public' in request.form:
        media = Media.query.filter_by(id=request.form["id"]).first()
        if media is None:
            abort(404)
        elif media.idU != current_user.id:
            return "ACCESS FORBIDEN: not your image", 403
        else:
            if request.form['public'] == "true":
                pub = True
            else:
                pub = False
            media.public = pub
            db.session.commit()

    else:
        return "err: missing argument.s", 500
