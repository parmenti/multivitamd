import os
import shutil
import sys
import zipfile

import pypandoc
from flask import request, render_template, url_for, flash, abort
from flask_login import current_user
from werkzeug.utils import redirect, secure_filename
from yaml import load, FullLoader, dump

from app import temp_dir, db, files_dir, version as fileVersion
from app.form import NewFileForm, ImportFileForm
from app.modelControler.controlers import latestVersionIdF, exportVersions, idByDirectory, authorizeAccessFile, \
    shareFileById, unshareFileWith, directoryById, unshareFileEveryone
from app.modelControler.models import File, Share, Version
from app.process.read import getLanguages, read, getFile
from app.process.tools import rand, createFile, make_archive
from app.process.write import update, constructYAML

from flask_babel import _


# Displays documents
def docDocuments(type):
    if type == "private":
        f = File.query.filter_by(idU=int(current_user.id))

    elif type == "shared":
        f = (File.query.join(Share, File.id == Share.idF).filter(Share.idU == int(current_user.id)))

    else:
        return redirect(url_for('documents', type="private"))

    # Order by
    order = "recent"
    if "orderFiles" in request.cookies:
        order = request.cookies["orderFiles"]
    if order == "recent-1":
        f = f.order_by(File.create.asc()).all()
    elif order == "alpha":
        f = f.order_by(File.directory.asc()).all()
    elif order == "alpha-1":
        f = f.order_by(File.directory.desc()).all()
    else:
        f = f.order_by(File.create.desc()).all()

    v = []
    for row in f:
        v.append(latestVersionIdF(row.id))
    return render_template('files.html', type=type, file=f, version=v, order=order)


# Create or import a document from archive
def docCreate():
    form = NewFileForm()
    imp = ImportFileForm()
    if imp.submitImp.data:
        err = False
        # Get file from form and saves it
        filename = secure_filename(imp.source.data.filename)
        imp.source.data.save(temp_dir + "/" + filename)
        # Create new file directory
        tempDirectory = str(rand())
        os.mkdir(temp_dir + "/" + tempDirectory)
        # Unzip file
        zip = zipfile.ZipFile(os.path.join(temp_dir, filename))
        zip.extractall(temp_dir + "/" + tempDirectory)
        # Get directory name
        directoryName = os.listdir(temp_dir + "/" + tempDirectory)[0]
        if os.path.isfile(temp_dir + "/" + tempDirectory + "/" + directoryName + "/data.txt"):
            f = open(temp_dir + "/" + tempDirectory + "/" + directoryName + "/data.txt", "r")
            data = load(f, Loader=FullLoader)
            f.close()

            # Checks if data file is correctly made
            if data is None or 'versions' not in data:
                err = True
                flash(_("Cette archive n'a pas été reconnue. Assurez vous qu'il s'agit bien d'une archive MultivitaMD "
                        "en .zip [data.txt corrompu]"))

            newDirectoryName = directoryName + str(rand())
            shutil.move(temp_dir + "/" + tempDirectory + "/" + directoryName, files_dir + "/" + newDirectoryName)
            versions = data['versions']

            # create fil in db to access file id
            file = File(idU=current_user.id, directory=newDirectoryName)
            db.session.add(file)
            db.session.commit()

            # Request file
            file = File.query.filter_by(directory=newDirectoryName).first()

            # Add versions
            for v in versions:
                version = Version(idU=current_user.id, idF=file.id, path=v["path"], version=v["version"],
                                  delta=v["delta"], create=v["create"])
                db.session.add(version)
            db.session.commit()
        else:
            err = True
            flash(_("Cette archive n'a pas été reconnue. Assurez vous qu'il s'agit bien d'une archive MultivitaMD en "
                    ".zip [data.txt manquant]"))
            print('DATA MISSING')
        if err:
            return redirect(url_for('create'))
        else:
            flash(_("Document importé avec succès."))
            return redirect(url_for("filesDefault"))

    elif form.submitNew.data:
        err = False
        if form.lang0.data == form.lang1.data:
            err = True
            flash(_("La première et la deuxième langue doivent être différentes."))
        if form.lang0.data == form.lang2.data:
            err = True
            flash(_("La première et la troisième langue doivent être différentes."))
        if form.lang2.data == form.lang1.data:
            err = True
            flash(_("La deuxième et la troisième langue doivent être différentes."))
        if err:
            return redirect(url_for('create'))
        else:
            paths = createFile(form.fileName.data, form.lang0.data, form.lang1.data, form.lang2.data, files_dir,
                               fileVersion)
            file = File(idU=current_user.id, directory=paths['directory'])
            db.session.add(file)
            db.session.commit()
            file = File.query.filter_by(directory=paths['directory']).first()
            version = Version(idF=file.id, idU=current_user.id, path=paths["fileName"], version=1)
            db.session.add(version)
            db.session.commit()
            version = Version.query.filter_by(idF=file.id).first()
            flash(_("%(name)s a été créé.", name=form.fileName.data))
            return redirect(url_for('documents', type="private"))
    else:
        return render_template('newFile.html', form=form, imp=imp, user=current_user)


# Export actual document's directory and some data (versions)
def docArchive():
    directory = request.form['directory']
    if os.path.isdir(files_dir + "/" + directory):
        file = File.query.filter_by(directory=directory).first()
        # Extract informations
        f = open(files_dir + "/" + directory + "/" + "data.txt", "w+")
        f.write(dump({"versions": exportVersions(directory)}))
        f.close()
        # Add a readme
        f = open(files_dir + "/" + directory + "/" + "0-README-LISMOI.txt", "w+")
        f.write(
            "This is an archive of a MultivitaMD document.\nPLEASE DO NOT UNZIP THIS ARCHIVE\nDO NOT MAKE ANY CHANGE "
            "IN THOSE FILES\n\nCeci est une archive d'un document MultivitaMD.\nMERCI DE NE PAS DÉZIPPER\nNE FAITES "
            "AUCUNE MODIFICATION DES DOCUMENTS")
        f.close()
        make_archive(files_dir + "/" + directory, temp_dir + "/" + directory + ".zip")
        return directory
    else:
        abort(404)


# Export document to a define type
def docExport():
    if 'directory' in request.form and 'output' in request.form:
        version = latestVersionIdF(idByDirectory(request.form['directory']))
        langs = getLanguages(files_dir, request.form['directory'], version.path)
        txt = read(langs, files_dir, request.form['directory'], version.path)
        # Create directory
        directory = request.form['directory'] + str(rand())
        os.mkdir(temp_dir + "/" + directory)
        filename = request.form['directory'] + "." + request.form['output']
        # Converts to 3 files
        output = request.form['output']

        pypandoc.convert_text(txt.get(langs[0]), output, format='md',
                              outputfile=temp_dir + "/" + directory + "/" + langs[0] + "-" + filename)
        pypandoc.convert_text(txt.get(langs[1]), output, format='md',
                              outputfile=temp_dir + "/" + directory + "/" + langs[1] + "-" + filename)
        pypandoc.convert_text(txt.get(langs[2]), output, format='md',
                              outputfile=temp_dir + "/" + directory + "/" + langs[2] + "-" + filename)
        # Compress folder
        shutil.make_archive(temp_dir + "/" + directory, 'zip', temp_dir + "/" + directory)
        # redirectDownload(directory)
        return directory


# Return document as the actual file (YAML)
def docGetDocument():
    if 'directory' in request.form:
        if 'versionId' in request.form:
            return getFile(files_dir, request.form['directory'], request.form['versionId'])
        else:
            return getFile(files_dir, request.form['directory'], "latest")
    else:
        sys.stderr.write("Error while accessing file. Parameter directory missing. - file\n")
        return "err"


# Update Document informations & create a new verion
def docUpdateDocument():
    if 'directory' in request.form and 'fileName' in request.form and 'lang0' in request.form and 'lang1' in request.form and 'lang2' in request.form:
        update(files_dir, request.form['directory'], request.form['fileName'], request.form['lang0'],
               request.form['lang1'], request.form['lang2'])
        return redirect(url_for('document'), code=307)
    else:
        sys.stderr.write("ERR: erreur à l'enregistrement des mises-à-jour\n")
        return "err"


# Save document => create a new version if necessary
def docSave():
    if 'file' in request.form and 'content' in request.form and 'currentLang' in request.form and 'sameLevelTitle' in request.form:
        dir = request.form['directory']
        constructYAML(request.form['content'], request.form['currentLang'], files_dir, dir,
                      request.form['file'], request.form['sameLevelTitle'])
        return redirect(url_for('document'), code=307)
    else:
        sys.stderr.write("ERR: erreur à l'enregistrement\n")
        return redirect(url_for('document'), code=505)


# Share document
def docShare():
    auth = authorizeAccessFile(request.form['directory'], files_dir)
    if auth is not True:
        return "<div class='alert alert-danger' role='alert'><i class='far fa-times-circle'></i> " \
               + _("Il semblerait que vous n'avez pas accès à ce document. Rechargez la page et rééssayez.") + "</div>"
    shared = shareFileById(request.form['idF'], request.form['username'])
    if shared == 0:
        return "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> " \
               + _("Document partagé avec %(usr)s avec succès.", usr=request.form['username']) + "</div>"
    elif shared == 2:
        return "<div class='alert alert-warning' role='alert'><i class='fas fa-exclamation-circle'></i> " \
               + _("Ce document est déjà partagé avec %(usr)s .", usr=request.form['username']) + "</div>"
    else:
        return "<div class='alert alert-warning' role='alert'><i class='fas fa-exclamation-circle'></i> " \
               + _("L'utilisateur.trice<i> %(usr)s </i>n'a pas été trouvé.e.", usr=request.form['username']) + "</div>"


# Unshare document
def docUnshare():
    auth = authorizeAccessFile(directoryById(request.form['idF']), files_dir)
    if auth is not True:
        return "<div class='alert alert-danger' role='alert'><i class='far fa-times-circle'></i> " \
               + _("Il semblerait que vous n'avez pas accès à ce document. Rechargez la page et rééssayez.") + "</div>"
    if 'idU' in request.form:

        if request.form['idU'] == "self":
            unshareFileWith(request.form['idF'], current_user.id)
            flash(_("Vous avez masqué un document."))
            return "success"
        else:
            unshareFileWith(request.form['idF'], request.form['idU'])
        return "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> Ce document n'est " \
               + _("Ce document n'est plus partagé avec cet utilisateur.") + "</div>"
    else:
        unshareFileEveryone(request.form['idF'])
        return "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> " \
               + _("Ce document n'est plus partagé.") + "</div>"
