from flask import render_template, request, abort
from flask_login import current_user

from app import db
from app.modelControler.models import User


# Administration page
def admAdministration():
    if current_user.role == "user":
        return "Access denied: not an admin", 403
    elif current_user.role == "admin":
        users = User.query.order_by(User.username.asc()).all()
        return render_template("admin.html", users=users)


# Delete an user by id
def admDeleteUser():
    if current_user.role != "admin":
        return "Access denied", 403
    id = request.form['id']
    user = User.query.filter_by(id=id).first()
    db.session.delete(user)
    db.session.commit()
    return "User deleted."


# Change user role
def admChangeRole(role):
    if current_user.role != "admin":
        return "Access denied", 403
    id = request.form['id']
    user = User.query.filter_by(id=id).first()
    user.role = role
    db.session.commit()
    return "Role updated."


# reste user's password
def admResetPassword():
    if current_user.role != "admin":
        return "Access denied", 403
    id = request.form['id']
    password = request.form['password']
    user = User.query.filter_by(id=id).first()
    user.set_password(password)
    db.session.commit()
    return "Password changed."
