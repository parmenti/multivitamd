import os
import shutil

from flask import url_for, flash
from flask_login import login_required, current_user, logout_user
from werkzeug.utils import redirect
from flask_babel import _

from app import db, login
from app.modelControler.models import File, Share, User, Version, Media


# return file path according to id
def directoryById(idF):
    return File.query.filter_by(id=idF).first().directory


# return file path according to id
def idByDirectory(dir):
    return File.query.filter_by(directory=dir).first().id


@login_required
def authorizeAccessFile(directory, uploads_dir):
    file = File.query.filter_by(directory=directory).first()
    if file is None:
        flash(_("Ce document n'existe pas ou n'existe plus. (DATABASE)"))
        return redirect(url_for('index'))
    else:
        version = latestVersionIdF(file.id)
        share = Share.query.filter_by(idU=current_user.id, idF=file.id).first()
        if version is None:
            flash(_("Il n'existe pas ou plus de version de ce document. (DATABASE)"))
            return redirect(url_for('index'))
        elif not os.path.isfile(uploads_dir + "/" + file.directory + "/" + version.path):
            flash(_("Ce document n'existe pas ou plus sur nos serveurs."))
            return redirect(url_for('index'))
        elif file.idU != current_user.id and share is None:
            flash(_("Vous n'avez pas accès à ce document."))
            return redirect(url_for('index'))
        else:
            return True


# retruns latest version using path
def latestVersionIdF(idF):
    return latestVersionGlobal(Version.query.filter_by(idF=idF).all())


@login_required
def latestVersionGlobal(versions):
    latest = None
    ver = 0
    for row in versions:
        if row.version > ver:
            ver = row.version
            latest = row
    return latest


def deleteFileDb(uploads_dir, directory, msg=True):
    f = File.query.filter_by(directory=directory).first()
    if os.path.exists(uploads_dir + "/" + directory):
        shutil.rmtree(uploads_dir + "/" + directory, ignore_errors=True)
    if f is not None:
        unshareFileEveryone(f.id)
        deleteVersionsIdF(f.id)
        db.session.delete(f)
        db.session.commit()
        if msg:
            flash(_("Ce fichier a été effacé."))
    else:
        if msg:
            flash(_("Échec: Ce fichier est introuvable"))


def deleteVersionsIdF(idF):
    vers = Version.query.filter_by(idF=idF).all()
    for row in vers:
        db.session.delete(row)
    db.session.commit()


@login_required
def deleteAccount(uploads_dir):
    files = File.query.filter_by(idU=current_user.id).all()
    for r in files:
        deleteFileDb(uploads_dir, r.directory, False)
    medias = Media.query.filter_by(idU=current_user.id).all()
    for r in medias:
        db.session.delete(r)
    db.session.delete(current_user)
    db.session.commit()
    flash(_("Compte supprimé."))
    return redirect(url_for("index"))


# Used to share files
# Caution, idUser is not the actual idU from Share - it is
@login_required
def shareFileById(idF, username):
    idU = User.query.filter_by(username=username).first()
    if idU is None:
        return 1
    else:
        check = Share.query.filter_by(idU=idU.id, idF=idF).first()
        if check is not None:
            return 2
        else:
            share = Share(idU=idU.id, idF=idF)
            db.session.add(share)
            db.session.commit()
            return 0


# used to unshare a file with everyone
@login_required
def unshareFileEveryone(idF):
    s = Share.query.filter_by(idF=idF).all()
    for row in s:
        db.session.delete(row)
    db.session.commit()


@login_required
def unshareFileWith(idF, idU):
    s = Share.query.filter_by(idF=idF, idU=idU).all()
    for row in s:
        db.session.delete(row)
    db.session.commit()


@login_required
def restoreVersion(uploads_dir, directory, idV):
    v = Version.query.filter_by(id=idV).first()
    max = v.version
    allVersions = Version.query.filter_by(idF=v.idF).all()
    # id of version to hide
    toHide = ""
    for row in allVersions:
        if row.version > max:
            # Deletes version file from directory
            if os.path.exists(uploads_dir + "/" + directory + "/" + row.path):
                os.remove(uploads_dir + "/" + directory + "/" + row.path)
            toHide += str(row.version) + "-"
            # Deletes version from db
            db.session.delete(row)
    db.session.commit()
    return toHide


# Used to export version
@login_required
def exportVersions(directory):
    versions = Version.query.filter_by(idF=idByDirectory(directory))
    list = []
    for r in versions:
        list.append({"version": r.version, "path": r.path, "delta": r.delta, "create": r.create})
    return list


@login_required
def checkImageAccess(path):
    media = Media.query.filter_by(path=path).first()
    if media is None:
        return False
    if media.public:
        return True
    return media.idU == current_user.id
