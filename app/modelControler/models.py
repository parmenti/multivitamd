from datetime import datetime

from flask_login import UserMixin, login_manager
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login


# Represents the User table
# Uses UserMixin for login methods
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    lang0 = db.Column(db.String(30), default="")
    lang1 = db.Column(db.String(30), default="")
    lang2 = db.Column(db.String(30), default="")
    role = db.Column(db.String(30), default="user")

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


# used by flask_login to load the user
@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class File(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    idU = db.Column(db.Integer, db.ForeignKey('user.id'))
    directory = db.Column(db.String(60), unique=True)
    create = db.Column(db.DateTime, default=datetime.now)

    def __repr__(self):
        return '<File {}>'.format(self.name)


class Version(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    idF = db.Column(db.Integer, db.ForeignKey('file.id'))
    # Represents the user who made the update
    idU = db.Column(db.Integer, db.ForeignKey('user.id'))
    path = db.Column(db.String(60))
    version = db.Column(db.Integer, default=1)
    delta = db.Column(db.Integer, default=0)
    create = db.Column(db.DateTime, default=datetime.now)

    def __repr__(self):
        return '<{' + str(self.id) + '}Version n ' + str(self.version) + ' of file n ' + str(self.idF) + '>'


class Share(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    idU = db.Column(db.Integer, db.ForeignKey('user.id'))
    idF = db.Column(db.Integer, db.ForeignKey('file.id'))

    def __repr__(self):
        return '<Share file n ' + self.idF + ' with ' + self.idU + '>'


class Media(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    idU = db.Column(db.Integer, db.ForeignKey('user.id'))
    name = db.Column(db.String(60))
    path = db.Column(db.String(60), unique=True)
    public = db.Column(db.Boolean, default=False)
    create = db.Column(db.DateTime, default=datetime.now)

    def __repr__(self):
        return '<Media {}>'.format(self.name)