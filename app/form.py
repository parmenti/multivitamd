from flask_wtf import FlaskForm, validators
from flask_babel import _, lazy_gettext as _l
from wtforms import StringField, PasswordField, BooleanField, SubmitField, FileField
from wtforms.validators import DataRequired, EqualTo


class LoginForm(FlaskForm):
    username = StringField(validators=[DataRequired()])
    password = PasswordField(validators=[DataRequired()])
    remember_me = BooleanField()
    submit = SubmitField(_l('Se connecter'))


class RegistrationForm(FlaskForm):
    username = StringField(validators=[DataRequired()])
    email = StringField(validators=[DataRequired()])
    password = PasswordField(validators=[DataRequired()])
    password2 = PasswordField(validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField(_l('Créer le compte'))


class NewFileForm(FlaskForm):
    fileName = StringField(validators=[DataRequired()])
    lang0 = StringField(validators=[DataRequired()])
    lang1 = StringField(validators=[DataRequired()])
    lang2 = StringField(validators=[DataRequired()])
    submitNew = SubmitField(_l('Créer le document'))


class ImportFileForm(FlaskForm):
    source = FileField(validators=[DataRequired()])
    submitImp = SubmitField(_l('Importer le document'))


class EditAccountForm(FlaskForm):
    username = StringField(validators=[DataRequired()])
    email = StringField(validators=[DataRequired()])
    oldPassword = PasswordField(validators=[DataRequired()])
    password = PasswordField()
    password2 = PasswordField(validators=[EqualTo('password')])
    lang0 = StringField()
    lang1 = StringField()
    lang2 = StringField()
    submit = SubmitField(_l('Mettre à jour'))
    delete = SubmitField(_l('Suprimer le compte'))
