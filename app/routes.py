from flask_babel import _
from flask_login import login_required
from flask import redirect, url_for, send_file

from app import app
from app.errorMessages import errorTab
from app.modelControler.controlers import deleteFileDb, restoreVersion
from app.process.read import getFileInfos

from app.routeProcess.account import *
from app.routeProcess.documents import *
from app.routeProcess.medias import *
from app.routeProcess.admin import *


def password():
    return str(rand() * rand()) + str(rand() * rand())


app.jinja_env.globals.update(password=password)


@app.template_filter('formError')
def formError(err):
    return errorTab[err]


@app.template_filter('fileInfos')
def fileInfos(file):
    return getFileInfos(files_dir, file)


@app.template_filter('formTime')
def _jinja2_filter_formTime(date, fmt=None):
    if date is None:
        return "ERR"
    else:
        return date.strftime("%H:%M %d/%m/%Y")


@app.template_filter('idToUsername')
def idToUsername(idU):
    if idU is None:
        return "Error - no version found"
    else:
        u = User.query.filter_by(id=idU).first()
        return u.username


@app.template_filter('sharedWith')
def _jinja2_filter_sharedWith(idF):
    return Share.query.filter_by(idF=idF).all()


@app.template_filter('deltaInterprter')
def deltaInterprter(version):
    if version.version == 1:
        return _("Création du document")
    elif version.delta == 0:
        return _("Modification du nom du document ou des langues")
    else:
        return _("%(num)s caractère.s modifié.s", num=str(version.delta))


@app.route('/logout/')
def logout():
    return accLogout()


@app.route('/register/', methods=['GET', 'POST'])
def register():
    return accRegister()


@app.route('/login/', methods=['GET', 'POST'])
def login():
    return accLogin()


@app.route('/account/', methods=['GET', 'POST'])
@login_required
def account():
    return accAccount()


@app.route('/admin/')
@login_required
def administration():
    return admAdministration()


@app.route('/files/documents/')
def filesDefault():
    return redirect(url_for('documents', type="private"))


@app.route('/files/documents/<type>/')
@login_required
def documents(type):
    return docDocuments(type)


@app.route('/files/medias/<type>/')
@login_required
def medias(type):
    return medMedias(type)


@app.route('/files/documents/create/', methods=['GET', 'POST'])
@login_required
def create():
    return docCreate()


@app.route('/files/documents/archive/', methods=['POST'])
@login_required
def archive():
    return docArchive()


@app.route('/files/documents/export/', methods=['POST'])
@login_required
def export():
    return docExport()


@app.route('/files/documents/export/download/<directory>/', methods=['GET'])
@login_required
def redirectDownload(directory):
    #return send_from_directory(temp_dir, directory + ".zip", as_attachment=True, cache_timeout=0)
    return send_file(os.path.join(app.root_path, '..', app.instance_path, 'temporary', directory + ".zip"))


# We use the directory in the URL in order to show latest version
@app.route('/files/documents/edit/<directory>/', methods=['GET', 'POST'])
@login_required
def editer(directory):
    auth = authorizeAccessFile(directory, files_dir)
    if auth is not True:
        return auth
    return render_template('editor.html', directory=directory)


# We use the directory in the URL in order to show latest version
@app.route('/files/documents/explore/<directory>/', methods=['GET', 'POST'])
@login_required
def explore(directory):
    auth = authorizeAccessFile(directory, files_dir)
    if auth is not True:
        return auth
    else:
        f = File.query.filter_by(directory=directory).first()
        v = Version.query.filter_by(idF=f.id).order_by(Version.version.asc()).all()
        return render_template('versionsExplorer.html', file=f, versions=v)


@app.route('/md-example/')
def example():
    flash(_("Ceci est une démonstration. Pour utilisez toutes les fonctionnalités de MultivitaMD, connectez vous."))
    return render_template('example.html')


# Sauvegarde du fichier
@app.route('/save/', methods=['POST'])
@login_required
def save():
    return docSave()


# Méthode d'accès au fichier, utilisée par js
@app.route('/files/getDocument/', methods=['POST'])
@login_required
def document():
    return docGetDocument()


# Adds to flash
@app.route('/flash/', methods=['POST'])
def createFlash():
    flash(request.form['flash'])
    return "SUCCESS"


# Update langs and file name
@app.route('/updateFile/', methods=['POST'])
@login_required
def updateDocument():
    return docUpdateDocument()


# Used in js to delete files
@app.route('/deletefile/', methods=['POST'])
def deleteFile():
    if 'path' in request.form:
        deleteFileDb(files_dir, request.form['path'])
    return "success"


# used in js to share files
@app.route('/sharefile/', methods=['POST'])
@login_required
def shareFile():
    return docShare()


# used in js to unshare files
@app.route('/unsharefile/', methods=['POST'])
@login_required
def unshareFile():
    return docUnshare()


# used in js to restore a version
@app.route('/restore/', methods=['POST'])
@login_required
def restore():
    if 'directory' in request.form and 'idV' in request.form:
        auth = authorizeAccessFile(request.form['directory'], files_dir)
        if auth is not True:
            return "err"
        else:
            toHide = restoreVersion(files_dir, request.form['directory'], request.form['idV'])
            return toHide


# used in js add an image
@app.route('/addImage/', methods=['POST'])
@login_required
def addImage():
    return medAddImage()


@app.route('/deleteImage/', methods=['POST'])
@login_required
def deleteImage():
    return medDelete()


@app.route('/accessImage/<path>/')
def accessImage(path):
    return medAccess(path)


@app.route('/publicStatutMedia/', methods=['POST'])
@login_required
def changePublicStatut():
    return medPublicStatut()


@app.route('/admin/deleteAccount/', methods=['POST'])
@login_required
def deleteUser():
    return admDeleteUser()


@app.route('/admin/role/make/<role>/', methods=['POST'])
@login_required
def changeRole(role):
    return admChangeRole(role)


@app.route('/admin/resetPassword/', methods=['POST'])
@login_required
def resetPassword():
    return admResetPassword()


@app.route('/')
def index():
    return render_template('home.html')


@app.route('/about/')
def about():
    return render_template('about.html')


# @app.route('/init/')
# def init():
#     from app import db
#     from app.modelControler.models import User

#     # Creates first admin
#     # TODO set a strong password to admin
#     user = User(username="admin", role="admin", email="admin@mail.com")
#     user.set_password("admin")
#     db.session.add(user)
#     db.session.commit()
#     return ""
