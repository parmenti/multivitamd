import os
import shutil
from datetime import datetime
from random import random

from flask import session
from werkzeug.utils import secure_filename
from yaml import dump


# Give id other langs in list
def getIdOtherLangs(currentLang, langs):
    res = []
    if langs[0] == currentLang:
        res.append(langs[1])
        res.append(langs[2])
    elif langs[1] == currentLang:
        res.append(langs[0])
        res.append(langs[2])
    else:
        res.append(langs[0])
        res.append(langs[1])
    return res


# Indique le niveau du titre, c-à-d le nomde de #
def titleLevel(string):
    if string.count("#") > 0:
        return "#" * string.count("#") + " "
    else:
        return ""


# Retire le niveau du titre, c-à-d les #
def removeTitle(string):
    if string is not None and string is not '':
        string = string.replace('# ', '')
        return string.replace('#', '')
    else:
        return ''


# Create a file and its directory
# The first file created is considered first version
def createFile(fileName, lang0, lang1, lang2, uploads_dir, vesrsion="0.0"):
    date = datetime.now().strftime('%Y-%m-%dT%H:%M')
    directory = secure_filename(fileName) + str(round(random() * 100000))
    file = "1-" + secure_filename(fileName) + ".yml"
    os.mkdir(uploads_dir + "/" + directory)
    f = open(uploads_dir + "/" + directory + "/" + file, "w+")
    down = {"down": {"id": 1, "content": {lang0: "", lang1: "", lang2: ""},
                     "lastUpdate": {lang0: ["add", date], lang1: ["add", date], lang2: ["add", date]}}}
    content = {"infos": {"title": fileName, "languages": {0: lang0, 1: lang1, 2: lang2}, "version": vesrsion},
               "file": down}
    f.write(dump(content))
    f.close()
    return {"directory": directory, "fileName": file}


def rand():
    return round(random() * 1000000)


# inspired by seanbehan.com
def make_archive(source, destination):
    base = os.path.basename(destination)
    name = base.split('.')[0]
    format = base.split('.')[1]
    archive_from = os.path.dirname(source)
    archive_to = os.path.basename(source.strip(os.sep))
    shutil.make_archive(name, format, archive_from, archive_to)
    shutil.move('%s.%s' % (name, format), destination)
