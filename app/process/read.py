import os

import yaml
from flask import session
from yaml import load, FullLoader

from app.modelControler.controlers import latestVersionIdF
from app.modelControler.models import File, Version


def getLanguages(uploads_dir, directory, path):
    if os.path.isfile(uploads_dir + "/" + directory + "/" + path):
        f = open(uploads_dir + "/" + directory + "/" + path, "r")
        file = load(f, Loader=FullLoader)
        file = file["infos"]['languages']
        langs = []
        for i in file:
            langs.append(file.get(i))
        f.close()
        return langs


def getFileInfos(uploads_dir, file):
    v = latestVersionIdF(file.id)
    if os.path.isdir(uploads_dir + "/" + file.directory):
        if v is not None and os.path.isfile(uploads_dir + "/" + file.directory + "/" + v.path):
            f = open(uploads_dir + "/" + file.directory + "/" + v.path, "r")
            file = load(f, Loader=FullLoader)
            f.close()
            res = []
            res.append(file["infos"]['title'])
            file = file["infos"]['languages']

            for i in file:
                res.append(file.get(i))
        else:
            res = ["???", "???", "???", "MvMd:err-missing-versions"]
    else:
        res = ["???", "???", "???", "MvMd:err-missing-directory"]
    return res


# TODO remove
def getFilename(uploads_dir, directory, path):
    v = latestVersionIdF()
    if os.path.isfile(uploads_dir + "/" + directory + "/" + path):
        f = open(uploads_dir + "/" + directory + "/" + path, "r")
        file = load(f, Loader=FullLoader)
        file = file["infos"]
        title = file.get('title')
        f.close()
        return title


def getFilenameFile(uploads_dir, file):
    v = latestVersionIdF(file.id)
    if os.path.isfile(uploads_dir + "/" + file.directory + "/" + v.path):
        f = open(uploads_dir + "/" + file.directory + "/" + v.path, "r")
        file = load(f, Loader=FullLoader)
        f.close()
        file = file["infos"]
        title = file.get('title')
        return title


# Get file but keeps using YAML
def getFile(uploads_dir, directory, versionId):
    if versionId == "latest":
        version = latestVersionIdF(File.query.filter_by(directory=directory).first().id)
    else:
        version = Version.query.filter_by(id=versionId).first()
    if version is None:
        raise ValueError('Error while accessing file. Version not found in database - getFile')
    if os.path.isfile(uploads_dir + "/" + directory + "/" + version.path):
        f = open(uploads_dir + "/" + directory + "/" + version.path, "r")
        file = load(f, Loader=FullLoader)
        return file
    else:
        raise ValueError('Error while accessing file. File not found - getFile')


# lit le document et le met sous format texte
def read(languages, uploads_dir, directory, path):
    if os.path.isfile(uploads_dir + "/" + directory + "/" + path):
        f = open(uploads_dir + "/" + directory + "/" + path)
        file = load(f, Loader=yaml.FullLoader)
        file = file['file']
        lg0 = ""
        lg1 = ""
        lg2 = ""

        while "down" in file:
            file = file.get("down")
            cont = file.get("content")
            lg0 += (cont.get(languages[0], "err"))

            lg1 += (cont.get(languages[1], "err"))

            lg2 += (cont.get(languages[2], "err"))
        f.close()
        return {languages[0]: lg0, languages[1]: lg1, languages[2]: lg2}
    else:
        return -1
