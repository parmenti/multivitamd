from datetime import datetime

from app.process.tools import titleLevel, removeTitle


class File:
    def __init__(self, lang0, lang1, lang2, dicoText, dicoTime, serverFile, sameLevelTitle):
        self.lang0 = lang0
        self.lang1 = lang1
        self.lang2 = lang2
        self.dicoText = dicoText
        self.dicoTime = dicoTime
        # Caution, this is a string, not a bool
        self.sameLevelTitle = sameLevelTitle
        self.today = datetime.now().strftime('%Y-%m-%dT%H:%M')
        self.serverFile = serverFile

    # Prepare a line
    # replace not used at this time
    def line(self, id, text0, update, action, replace):
        otherLang = False
        time1 = None
        time2 = None

        # Old line - there is a version in other langs
        if id in self.dicoText:
            text1 = self.dicoText[id][self.lang1]
            text2 = self.dicoText[id][self.lang2]
            # Added line - no version in other langs
        else:
            text1 = ""
            text2 = ""

        if (text1 == "" or text1 == "\n") and (text2 == "" or text2 == "\n"):
            if id in self.serverFile:
                text1 = self.serverFile[id]["content"][self.lang1]
                text2 = self.serverFile[id]["content"][self.lang2]

                otherLang = True
                time1 = self.serverFile[id]["lastUpdate"][self.lang1]
                time2 = self.serverFile[id]["lastUpdate"][self.lang2]

        if not update and id in self.dicoTime:
            time0 = self.dicoTime[id][self.lang0]
        else:
            time0 = [action, self.today]

        if time1 is None and time2 is None:
            if id in self.dicoTime:
                otherLang = True
                time1 = self.dicoTime[id][self.lang1]
                time2 = self.dicoTime[id][self.lang2]
            else:
                time1 = ["add", "2000-08-07T01:01"]
                time2 = ["add", "2000-08-07T01:01"]

        # Same text lvl
        if self.sameLevelTitle is True:
            text1 = titleLevel(text0) + removeTitle(text1)
            text2 = titleLevel(text0) + removeTitle(text2)

        res = {"id": id, "content": {self.lang0: text0, self.lang1: text1, self.lang2: text2},
               "lastUpdate": {self.lang0: time0, self.lang1: time1, self.lang2: time2}}
        return [res, otherLang]

    # remove line when all are empty
    def clean(self, file):
        for row in file:
            row0 = row["content"][self.lang0]
            row1 = row["content"][self.lang1]
            row2 = row["content"][self.lang2]

            if (row0 is "" or row0 is "\n") and (row1 is "" or row1 is "\n") and (row2 is "" or row2 is "\n"):
                file.remove(row)
            elif row["lastUpdate"][self.lang0][0] == "-" and row["lastUpdate"][self.lang1][0] == "-" and \
                    row["lastUpdate"][self.lang2][0] == "-":
                file.remove(row)
        return file

    def checkln(self, res):
        i = 0
        while i < len(res):
            row = res[i]
            if len(row["content"][self.lang0]) > 0 and row["content"][self.lang0][-1] is not "\n":
                row["content"][self.lang0] += "\n"

            if len(row["content"][self.lang1]) > 0 and row["content"][self.lang1][-1] is not "\n":
                row["content"][self.lang1] += "\n"

            if len(row["content"][self.lang2]) > 0 and row["content"][self.lang2][-1] is not "\n":
                row["content"][self.lang2] += "\n"

            if i < len(res) - 1:
                if row["content"][self.lang0] == "":
                    row["content"][self.lang0] = "¶"

                if row["content"][self.lang1] == "":
                    row["content"][self.lang1] = "¶"

                if row["content"][self.lang2] == "":
                    row["content"][self.lang2] = "¶"

            i += 1

        return res
