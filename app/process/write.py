import difflib
from datetime import datetime
import yaml
from diff_match_patch import *
from flask_login import current_user
from yaml import load, FullLoader

from app import db
from app.modelControler.controlers import latestVersionIdF, idByDirectory
from app.process.file import *
from app.process.read import getLanguages
from app.process.tools import *
from app.modelControler.models import File as dbFile, Version

dmp = diff_match_patch()
dmp.Diff_Timeout = 0
differ = difflib.Differ()


# update the file and languages
def update(uploads_dir, directory, fileName, lang0, lang1, lang2):
    fileDB = dbFile.query.filter_by(directory=directory).first()
    fileDB.name = fileName
    fileDB.lang0 = lang0
    fileDB.lang1 = lang1
    fileDB.lang2 = lang2

    oldVersion = latestVersionIdF(fileDB.id)

    f = open(uploads_dir + "/" + directory + "/" + oldVersion.path)
    file = load(f, Loader=yaml.FullLoader)
    f.close()
    infos = file['infos']
    # We collect the old keys
    olang0 = infos["languages"][0]
    olang1 = infos["languages"][1]
    olang2 = infos["languages"][2]

    # We update file infos
    infos["title"] = fileName
    infos["languages"][0] = lang0
    infos["languages"][1] = lang1
    infos["languages"][2] = lang2

    # We will now change all key, using old keys
    edited = []
    file = file['file']
    while "down" in file:
        file = file.get("down")
        cont = file.get("content")
        upd = file.get("lastUpdate")
        id = file.get("id")
        if cont is not None:
            edited.append({"content": {lang0: cont[olang0], lang1: cont[olang1], lang2: cont[olang2]},
                           "lastUpdate": {lang0: upd[olang0], lang1: upd[olang1], lang2: upd[olang2]}, "id": id})

    # We create the dictionnary using edited
    edited.reverse()
    down = edited[0]
    edited.pop(0)
    for row in edited:
        row.update({"down": down})
        tmp = row
        down = tmp

    newVersionNumber = oldVersion.version + 1
    newPath = str(newVersionNumber) + "-" + fileDB.name + ".yml"
    newVersion = Version(idF=fileDB.id, idU=current_user.id, path=newPath, version=newVersionNumber, delta=0)
    db.session.add(newVersion)

    completedFile = {"infos": infos, "file": {"down": down}}
    f = open(uploads_dir + "/" + directory + "/" + newPath, "w+")
    f.write("")
    f.write(dump(completedFile))
    f.close()

    db.session.commit()


# construction du fichier en YAML
def constructYAML(currenttxt, currentlang, uploads_dir, directory, oldfile, sameLevelTitle):
    # Accessing latest version
    version = latestVersionIdF(idByDirectory(directory))

    fullPath = uploads_dir + "/" + directory + "/" + version.path

    # Accessing file languages
    langs = getLanguages(uploads_dir, directory, version.path)

    # Recherche de l'identifiant des langues
    otherlangs = getIdOtherLangs(currentlang, langs)
    lang1 = otherlangs[0]
    lang2 = otherlangs[1]

    curtxt = currenttxt

    # Récupération des anciennes lignes pour la langue en édition
    f = open(fullPath, "r")
    sfile = load(f, Loader=FullLoader)
    fileInfos = sfile["infos"]

    oldfile = yaml.safe_load(oldfile)
    file = oldfile['file']
    newText = ""
    dicoText = {}
    dicoTime = {}
    while "down" in file:
        file = file.get("down")
        tmp = file.get("content")
        if tmp is not None and tmp[currentlang] is not None:
            newText += tmp[currentlang]
            dicoText.update({
                file.get("id"): {currentlang: tmp.get(currentlang), lang1: tmp.get(lang1), lang2: tmp.get(lang2)}})
            dicoTime.update({file.get("id"): {currentlang: file.get("lastUpdate").get(currentlang),
                                              lang1: file.get("lastUpdate").get(lang1),
                                              lang2: file.get("lastUpdate").get(lang2)}})
    f.close()

    # patches file and indicate delta between current text and server text
    tmp = patchFilesVanilla(uploads_dir, fullPath, curtxt, oldfile, currentlang)
    if tmp is not None and tmp[1] > 0:
        curtxt = tmp[0]
        delta = tmp[1]
        file = compareTexts(curtxt, dicoText, dicoTime, sfile, currentlang, lang1, lang2, sameLevelTitle)

        # file = patchFiles(uploads_dir, path, file, currentlang)

        if len(file) != 0:
            file.reverse()
            down = file[0]
            del file[0]
            for row in file:
                row.update({"down": down})
                tmp = row
                down = tmp

        else:
            down = {"down": {"id": 1, "content": {currentlang: "", lang1: "", lang2: ""},
                             "lastUpdate": {currentlang: ["0", "2000-08-07T01:01"], lang1: ["0", "2000-08-07T01:01"],
                                            lang2: ["0", "2000-08-07T01:01"]}}}

        newVersionNumber = version.version + 1
        newPath = str(newVersionNumber) + "-" + fileInfos["title"] + "-" + str(rand()) + ".yml"
        newVersion = Version(idF=idByDirectory(directory), idU=current_user.id, path=newPath, version=newVersionNumber,
                             delta=delta)
        db.session.add(newVersion)
        # commit is made later in order to make sure file is saved
        completedFile = {"infos": fileInfos, "file": {"down": down}}
        f = open(uploads_dir + "/" + directory + "/" + newPath, "w+")
        f.write(dump(completedFile))
        f.close()

        db.session.commit()

    return "err"


# Compares the new text with the old one and indicates the change type and date.
def compareTexts(newText, dicoTxt, dicoTime, serverFile, lang0, lang1, lang2, sameLevelTitle):
    down = serverFile["file"]
    serverFile = {}
    while "down" in down:
        down = down["down"]
        serverFile.update({down["id"]: {"content": down["content"], "lastUpdate": down["lastUpdate"]}})

    f = File(lang0, lang1, lang2, dicoTxt, dicoTime, serverFile, sameLevelTitle)
    old = list(dicoTxt.keys())
    server = list(serverFile.keys())
    new = newText.split("\n")
    i = 0
    while i < len(new) - 1:
        new[i] += "\n"
        i += 1

    content = []
    lastUpdate = []
    res = []

    n = 0
    o = 0
    s = 0

    while n < len(new) or o < len(old) or s < len(server):

        if o > len(old) - 1 and n > len(new) - 1:
            res.append(f.line(server[s], "", False, "0", None)[0])
            s += 1
        elif o > len(old) - 1:
            if s < len(server):
                res.append(f.line(server[s], new[n], True, "add", None)[0])
            else:
                res.append(f.line(rand(), new[n], False, "add", None)[0])
            n += 1
            s += 1
        elif n > len(new) - 1:
            res.append(f.line(old[o], "", False, "del", dicoTxt[old[o]])[0])
            o += 1
            s += 1
        else:
            diffs = dmp.diff_main(dicoTxt[old[o]][lang0], new[n])
            dmp.diff_cleanupSemantic(diffs)
            # Check if new version works better don't delete next row in case of emergency
            # diffs = dmp.diff_lineMode(dicoTxt[old[o]][lang0], new[n], 0)

            none = 0
            delete = 0
            add = 0

            added = []

            for row in diffs:
                if row[0] == 0:
                    none += 1
                elif row[0] == -1 and row[1] != "":
                    delete += 1
                elif row[0] == 1 and row[1] != "":
                    added.append(row[1])
                    add += 1
            # no changes
            if none > 0 and delete < 1 and add < 1:
                res.append(f.line(old[o], new[n], False, "0", dicoTxt[old[o]])[0])
                n += 1
                o += 1
                s += 1
            elif add == 1 and delete == 1 and none == 1:

                if dicoTxt[old[o]][lang0] == "":
                    res.append(f.line(old[o], new[n], True, "edit", dicoTxt[old[o]])[0])
                    # check
                    o += 1
                    s += 1
                else:
                    tmp = f.line(old[o], new[n], True, "add", dicoTxt[old[o]])
                    res.append(tmp[0])
                    if tmp[1]:
                        o += 1
                        s += 1
                n += 1
            # Simple edit
            elif none > 0 and delete > 0 and add > 0:
                res.append(f.line(old[o], new[n], True, "edit", dicoTxt[old[o]])[0])
                n += 1
                o += 1
                s += 1
            # Two types of edit
            elif add > 0 and (delete > 0 or none > 0):
                # We check if the added part was the next row
                wasOld = False
                for row in added:
                    if o + 1 < old.__len__() and row.replace("\n", "") == dicoTxt[old[o + 1]][lang0].replace("\n",
                                                                                                             ""):
                        wasOld = True
                # Edited using a previous string
                if wasOld:
                    res.append(f.line(old[o], new[n], True, "edit", dicoTxt[old[o]])[0])
                    o += 1
                    s += 1
                    res.append(f.line(old[o], "", True, "del", dicoTxt[old[o]])[0])
                    n += 1
                    o += 1
                    s += 1
                # Edit
                else:
                    res.append(f.line(old[o], new[n], True, "edit", dicoTxt[old[o]])[0])
                    n += 1
                    o += 1
                    s += 1
            # Simple add
            elif add > 0 and delete < 1 and none < 1:
                tmp = f.line(old[o], new[n], True, "add", dicoTxt[old[o]])
                res.append(tmp[0])
                n += 1
                if tmp[1]:
                    o += 1
                    s += 1
            # Only delete
            elif delete and not add and not none:
                res.append(f.line(old[o], "", True, "del", dicoTxt[old[o]])[0])
                n += 1
                o += 1
                s += 1
            # Delete
            elif delete == 1 and none >= 0 and add < 1:
                res.append(f.line(old[o], new[n], True, "edit", dicoTxt[old[o]])[0])
                n += 1
                o += 1
                s += 1
            # Other cases, considere d edited
            else:
                res.append(f.line(old[o], new[n], True, "edit", dicoTxt[old[o]])[0])
                n += 1
                o += 1
                s += 1
    # res = f.checkln(res)
    res = f.clean(res)
    return res


# Compares the document with the online version and patches it. Useful in case of concurrent editing.
def patchFilesVanilla(uploads_dir, filePath, newText, oldCopy, currentLang):
    f = open(filePath)
    serverFile = yaml.safe_load(f)
    f.close()
    if serverFile["file"] is not None:
        serverFile = serverFile["file"]

        serverText = ""
        serverUpdates = []
        serverId = []
        oldText = ""
        newUpdates = []
        newId = []
        replace = []
        while "down" in serverFile:
            serverFile = serverFile["down"]
            if serverFile is not None and "content" in serverFile and 'lastUpdate' in serverFile and "id" in serverFile:
                serverText += serverFile["content"][currentLang]
                upd = serverFile["lastUpdate"]
                serverUpdates.append(upd)
                serverId.append(serverFile["id"])

        oldCopy = oldCopy['file']
        while "down" in oldCopy:
            oldCopy = oldCopy["down"]
            if oldCopy is not None and "content" in oldCopy:
                oldText += oldCopy["content"][currentLang]

        diffs = dmp.diff_main(oldText, newText)
        delta = dmp.diff_levenshtein(diffs)

        patch = dmp.patch_make(oldText, diffs)
        # applies patch to server version
        applied = dmp.patch_apply(patch, serverText)

        return [applied[0], delta]
