import os
import shutil
import sys
from time import sleep


def cleanFiles(path):
    for root, dirs, files in os.walk(path):
        for file in files:
            os.remove(os.path.join(root, file))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
    print("Cleaned export directory")
    # Clean every 10 hours
    sleep(36000)
    cleanFiles(path)


if os.path.isdir(sys.argv[1] + "/temporary/"):
    cleanFiles(sys.argv[1] + "/temporary/")
else:
    raise ValueError('ERROR: Temporary file not found')
