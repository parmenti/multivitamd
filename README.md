# MultivitaMD

MultivitaMD is a project taking place in the [PIAF project](https://penseeinformatique.frama.site/).

The main goal of MultivitaMD is to handle mulilangual documents.

**Structure**
This project is articulated around an *routes.py* file.
Flask is used to generate a website using differnt templates:
* *login.html* is the... login page
* *register.html* is used to create a new account
* *files.html* displays your files and those you have shared
*  *newFile.html* is the "create new file" page
*  *home.html* is the homepage
*  *upload.html* is the upload page or "open a file" page
*  *form.html* is the main page. This is where the file is displayed and where you can edit it
*  *structure.html* is the structure of those pages, including header, nav bar and the css and js tags

All those templates use jinja.

Css and js files are stored inside the static repository.


The *routes.py* file uses diferent functions in order to access the webpages and to control the documents.

The main functions are *read* and *constructYAML*. Thoses are stored under *app/process* in *read.py* and *write.py*

The first one is used in order to create three strings used to create human friendly displays.
This function is mostly used by */files/edit* in order to prepare the human display.

The second is used in order to prepare the file under YAML format and to save the chnages.

Js is used for multiple reasons.
It is separated in two files:
*  *editor.js* used to save the file, update the view using the */file/* page and prepare a human friendly display using markdown tags.
* *scroll.js* used to sychronize different scrollbars

Since version 0.2.0 users have the possibility to create an account. In order to handle this, a database was ceated.
During development, *app.db* is used.

Our project also uses other people's work:
* [*showdown.js*](http://showdownjs.com/) used to convert md to HTML
* [*Mousetrap*](https://craig.is/killing/mice) used to create shortcuts in js
* Flask's libraries 
* More details in _requirements.txt_
