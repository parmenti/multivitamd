"""empty message

Revision ID: e1654a78d910
Revises: 
Create Date: 2020-06-12 10:45:06.151155

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e1654a78d910'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=64), nullable=True),
    sa.Column('email', sa.String(length=120), nullable=True),
    sa.Column('password_hash', sa.String(length=128), nullable=True),
    sa.Column('lang0', sa.String(length=30), nullable=True),
    sa.Column('lang1', sa.String(length=30), nullable=True),
    sa.Column('lang2', sa.String(length=30), nullable=True),
    sa.Column('role', sa.String(length=30), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_user_email'), 'user', ['email'], unique=True)
    op.create_index(op.f('ix_user_username'), 'user', ['username'], unique=True)
    op.create_table('file',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('idU', sa.Integer(), nullable=True),
    sa.Column('directory', sa.String(length=60), nullable=True),
    sa.Column('create', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['idU'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('directory')
    )
    op.create_table('media',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('idU', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=60), nullable=True),
    sa.Column('path', sa.String(length=60), nullable=True),
    sa.Column('public', sa.Boolean(), nullable=True),
    sa.Column('create', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['idU'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('path')
    )
    op.create_table('share',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('idU', sa.Integer(), nullable=True),
    sa.Column('idF', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['idF'], ['file.id'], ),
    sa.ForeignKeyConstraint(['idU'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('version',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('idF', sa.Integer(), nullable=True),
    sa.Column('idU', sa.Integer(), nullable=True),
    sa.Column('path', sa.String(length=60), nullable=True),
    sa.Column('version', sa.Integer(), nullable=True),
    sa.Column('delta', sa.Integer(), nullable=True),
    sa.Column('create', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['idF'], ['file.id'], ),
    sa.ForeignKeyConstraint(['idU'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('version')
    op.drop_table('share')
    op.drop_table('media')
    op.drop_table('file')
    op.drop_index(op.f('ix_user_username'), table_name='user')
    op.drop_index(op.f('ix_user_email'), table_name='user')
    op.drop_table('user')
    # ### end Alembic commands ###
